package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"

	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/utils"
	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/wallpaper"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Check for INSTANCE_URL and MASTODON_TOKEN
	utils.CheckEnv()

	var scheduleEnable bool = false

	scheduleEnableEnv := os.Getenv("SCHEDULE_ENABLE")
	if scheduleEnableEnv == "true" {
		scheduleEnable = true
	}

	wallpaper.PostWallpaper(scheduleEnable)
}
