package mastodon

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/models"
	"codeberg.org/tomkoid/wallpaper-bot-mastodon/internal/utils"
)

func PostStatus(wl models.Wallpaper, mediaID string, scheduleTime *string) error {
	url := fmt.Sprintf("%s/api/v1/statuses", os.Getenv("INSTANCE_URL"))
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return err
	}

	if wl.Purity != "sfw" {
		return errors.New("Wallpaper is not SFW")
	}

	q := req.URL.Query()

	statusText := fmt.Sprintf(
		"👁️ Views: %d\n⭐ Favorites: %d\n🍎 Category: %s\n🖼️ Resolution: %s\n🔗 Wallpaper on Wallhaven: %s\n\n#wallpaper #wallpapers #bot",
		wl.Views,
		wl.Favorites,
		wl.Category,
		wl.Resolution,
		wl.Url,
	)

	q.Add("media_ids[]", mediaID)
	q.Add("status", statusText)
	q.Add("visibility", os.Getenv("VISIBILITY"))

	if scheduleTime != nil {
		q.Add("scheduled_at", *scheduleTime)
	}

	req.URL.RawQuery = q.Encode()

	req.Header.Set("Authorization", "Bearer "+os.Getenv("MASTODON_TOKEN"))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		fmt.Println("Wallpaper posted successfully!")
	} else {
		errorMsg := errors.New(fmt.Sprintf("Error posting wallpaper! Status: %s", resp.Status))
		utils.LogError(errorMsg, false)
		return errorMsg
	}

	return nil
}
