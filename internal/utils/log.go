package utils

import (
	"fmt"
	"log"
	"os"
	"sync"
)

var l = log.New(os.Stderr, "", 0)

func SendNtfyNotification(request NtfyRequest, wg *sync.WaitGroup) {
	if os.Getenv("NTFY_ROOM") != "" {
		go func() {
			err := ntfy(request)
			if err != nil {
				l.Println(err)
			}

			if wg != nil {
				wg.Done()
			}
		}()
	} else {
		if wg != nil {
			wg.Done()
		}
	}
}

func Log(message string) {
	fmt.Println(message)
}

func LogError(message error, exit bool) {
	l.Println(message)

	if !exit {
		SendNtfyNotification(NtfyRequest{
			Title:    "Wallpaper Bot",
			Body:     "Error: " + message.Error(),
			Priority: 4,
		}, nil)
	} else {
		wg := &sync.WaitGroup{}

		wg.Add(1)
		SendNtfyNotification(NtfyRequest{
			Title:    "Wallpaper Bot",
			Body:     "Error(exit): " + message.Error(),
			Priority: 5,
		}, wg)

		wg.Wait()

		if exit {
			os.Exit(1)
		}
	}
}
