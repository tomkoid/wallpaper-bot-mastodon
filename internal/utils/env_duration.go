package utils

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

func ParseEnvDuration(envVar string) (*time.Duration, error) {
	if envVar == "" {
		return nil, errors.New("ParseEnvDuration: var is empty")
	}

	// minutes default
	var durationWithoutExtStr string
	var durationWithoutExt time.Duration
	var durationUnit time.Duration

	// get correct durationUnit
	switch {
	case strings.HasSuffix(envVar, "s"):
		durationUnit = time.Second
		durationWithoutExtStr = strings.Split(envVar, "s")[0]
	case strings.HasSuffix(envVar, "m"):
		durationUnit = time.Minute
		durationWithoutExtStr = strings.Split(envVar, "m")[0]
	case strings.HasSuffix(envVar, "h"):
		durationUnit = time.Hour
		durationWithoutExtStr = strings.Split(envVar, "h")[0]
	case strings.HasSuffix(envVar, "d"):
		durationUnit = time.Hour * 24
		durationWithoutExtStr = strings.Split(envVar, "d")[0]
	default:
		durationUnit = time.Second
	}

	durationWithoutExtInt, err := strconv.Atoi(durationWithoutExtStr)
	if err != nil {
		LogError(errors.New("could not convert duration to int"), true)
	}

	durationWithoutExt = time.Duration(durationWithoutExtInt)

	overallDuration := durationWithoutExt * durationUnit

	return &overallDuration, nil
}
