package utils

import (
	"testing"
	"time"
)

func TestParseEnvDuration(t *testing.T) {
	// 30s
	expResult := time.Duration(30 * time.Second)
	result, err := ParseEnvDuration("30s")

	if err != nil {
		t.Errorf("30s err: %v", err)
	}

	if *result != expResult {
		t.Errorf("30s = %d; want %d", result, expResult)
	}

	// 4h
	expResult = time.Hour * 4
	result, err = ParseEnvDuration("4h")

	if err != nil {
		t.Errorf("1h err: %v", err)
	}

	if *result != expResult {
		t.Errorf("1h = %d; want %d", result, expResult)
	}

	// 1d
	expResult = time.Duration(24 * time.Hour)
	result, err = ParseEnvDuration("1d")

	if err != nil {
		t.Errorf("1d err: %v", err)
	}

	if *result != expResult {
		t.Errorf("1d = %d; want %d", result, expResult)
	}
}
