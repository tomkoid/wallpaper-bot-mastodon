package utils

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

const CACHE_FILE = "./cache.txt"

func AddDateToCache(date string) error {
	checkCache()
	
	dat, err := os.ReadFile(CACHE_FILE)

	if err != nil {
		return err
	}

	body := string(dat)

	if body != "" {
		body = fmt.Sprintf("%s\n", body)
	}

	writeData := []byte(fmt.Sprintf("%s%s", body, date))
	err = os.WriteFile(CACHE_FILE, writeData, 777)

	return nil
}

func IsInCache(date string) (bool, error){
	checkCache()

	dat, err := os.ReadFile(CACHE_FILE)

	if err != nil {
		return false, err
	}

	body := string(dat)

	return strings.Contains(body, date), nil
}

func checkCache() {
	if _, err := os.Stat(CACHE_FILE); errors.Is(err, os.ErrNotExist) {
		os.Create("./cache.txt")
	}
}
